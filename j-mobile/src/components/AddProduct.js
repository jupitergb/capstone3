import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

function AddProduct({ fetchData }) {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [postImage, setPostImage] = useState("")


	const [showEdit, setShowEdit] = useState(false)

	const openEdit = () => {

		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
		setPostImage(null);
	}

	const handleFileUpload = async (e) => {
		
		const file = e.target.files[0];
		const base64 = await convertToBase64(file);
		setPostImage(base64)
	  }



	const saveProduct = (e) => {
		e.preventDefault();

		fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/products/create`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				image: postImage,
				description: description,
				price: price
			})
		})
			.then(res => res.json())
			.then(data => {

				if (data.status === true) {
					Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: data.message
					})
					closeEdit();
					fetchData();

				} else {
					Swal.fire({
						title: 'Error!',
						icon: 'error',
						text: data.details
					})
					closeEdit();
					fetchData();
				}
			})
	}


	return (
		<>
			<Button className='ms-auto me-5' variant="primary" size="lg" onClick={() => openEdit()}> + Create New </Button>

			{/*Add Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => saveProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Create a new Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={e => setName(e.target.value)}
								required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Image</Form.Label>
							<Form.Control
								id='file-upload'
								type="file"
								name="myFile"
								accept=".jpeg, .png, .jpg"
								onChange={(e) => handleFileUpload(e)}
								required />
						</Form.Group>



						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control
								as="textarea"
								rows="3"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={e => setPrice(e.target.value)}
								required />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>

			</Modal>
		</>
	)
}

export default AddProduct;

function convertToBase64(file){
	return new Promise((resolve, reject) => {
	  const fileReader = new FileReader();
	  fileReader.readAsDataURL(file);
	  fileReader.onload = () => {
		resolve(fileReader.result)
	  };
	  fileReader.onerror = (error) => {
		reject(error)
	  }
	})
  }
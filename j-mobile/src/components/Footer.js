

export default function Footer() {
  return (
    <footer className="bg-dark text-light mt-5">
        <div className="d-flex justify-content-center pt-3">
          <p className="fs-6">
            &copy; {new Date().getFullYear()} JMobile. All rights reserved.
          </p>
        </div>
    </footer>
  );
}
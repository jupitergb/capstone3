import React, { useState, useEffect, useContext } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Breadcrumb, Button, Card, Container, Modal } from 'react-bootstrap';
import { FaSearch, FaHeart, FaShoppingCart } from 'react-icons/fa';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';



export default function UserView({ productsData }) {

	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const { setCart } = useContext(UserContext);
	const [isLoading, setIsLoading] = useState(true);

	// Add state variables for the pop-up
	const [showPopup, setShowPopup] = useState(false);
	const [selectedProduct, setSelectedProduct] = useState(null);

	const fetchCart = () => {

		fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')} `
			}
		})
			.then(res => res.json())
			.then(data => {
				setCart(data);
			})
	}

	const handleAddToCart = (productId, productName) => {

		if (user.id === null) {
			Swal.fire({
				title: 'Not Logged In!',
				icon: 'info',
				text: `You are not logged in. Log in now to add items to your cart!`
			})

		} else {
			fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/add/${productId}/1`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
				.then(res => res.json())
				.then(data => {

					if (data.status === true) {
						Swal.fire({
							title: 'Success!',
							icon: 'success',
							text: `${productName} added to cart`
						})
						fetchCart();

					} else {
						Swal.fire({
							title: 'Error!',
							icon: 'error',
							text: data.error
						})
						fetchCart();
					}
				})

		}


	}

	useEffect(() => {
		if (products.length > 0) {
			setIsLoading(false); // Data is loaded
		}
	}, [products]);



	useEffect(() => {
		const productsArr = productsData.map((product, index) => {
			if (product.isActive === true) {
				return (
					<Card key={index} style={{ width: '18rem' }} className='m-3'>
						<Card.Body className='d-flex flex-column'>
							<Card.Img
								variant="top"
								src={product.image}
								style={{ objectFit: 'cover', margin: 'auto' }}
								className='p-3'
							/>
							<Card.Subtitle className='text-center mt-auto'>{product.name}</Card.Subtitle>
							<Card.Text className='text-center m-3 ' style={{ fontSize: '14px' }}>
								₱ {product.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}
							</Card.Text>
						</Card.Body>
						<div className='card-overlay'>
							<FaSearch
								className='icon'
								onClick={() => {
									// Show the pop-up and set the selected product
									setShowPopup(true);
									setSelectedProduct(product);
								}}
							/>

							{/* <FaHeart className='icon' /> */}
							<FaShoppingCart className='icon'
								onClick={() => handleAddToCart(product._id, product.name)} />
						</div>
					</Card>
				);
			} else {
				return null;
			}
		});

		setProducts(productsArr);

	}, [productsData]);

	return (
		<>
			<Container>
				<Breadcrumb className="mt-3">
					<Breadcrumb.Item href="/">Home</Breadcrumb.Item>
					<Breadcrumb.Item href="/products">Products</Breadcrumb.Item>
				</Breadcrumb>

				<Row className='justify-content-center'>
					{isLoading ? (
						// Loading component goes here
						<div className="spinner-border" role="status">
							<span className="sr-only"></span>
						</div>
					) : (
						// Render orders when data is available
						products
					)}
				</Row>

				{/* Add the pop-up */}
				<Modal show={showPopup} onHide={() => setShowPopup(false)}>
					<Modal.Header closeButton>
						<Modal.Title>Product Details</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{/* Display the selected product's information here */}
						{selectedProduct && (
							<div>
								<div className="text-center">
									<img
										src={selectedProduct.image}
										alt={selectedProduct.name}
										className="mx-auto d-block"
										style={{ maxWidth: '70%' }}
									/>
								</div>
								<h4>{selectedProduct.name}</h4>
								<p>Price: ₱ {selectedProduct.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</p>
								<p className='my-5'>{selectedProduct.description}</p>

								<div className="d-flex justify-content-around align-items-center">
									<Button
										variant="primary"
										onClick={() => {
											handleAddToCart(selectedProduct._id, selectedProduct.name);
											setShowPopup(false); // Close the pop-up after adding to cart
										}}
									>
										Add to Cart
									</Button>
									<Link
										to={`/single-product/${selectedProduct._id}`}
										className='px-0 pb-3'
										style={{ textDecoration: "underline", color: "gray" }}
									>
										View Product Details
									</Link>
								</div>
							</div>
						)}
					</Modal.Body>




				</Modal>
			</Container>
		</>
	);
}

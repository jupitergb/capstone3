import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Form, Card, Button } from 'react-bootstrap';
import ArchiveProduct from './ArchiveProduct';
import AddProduct from './AddProduct';
import UpdateProduct from './UpdateProduct';



export default function AdminView({ productsData, fetchData }) {


    const [products, setProducts] = useState([])

	const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
		if (products.length > 0) {
			setIsLoading(false); // Data is loaded
		}
	}, [products]);

    useEffect(() => {
        const productsArr = productsData.map(product => {
            return (
                <Card key={product._id} style={{ width: '18rem' }} className='m-3'>
                    <Card.Body className='d-flex flex-column'>
                        <Card.Img
                            variant="top"
                            src={product.image}
                            alt={product.name}
                            style={{ objectFit: 'cover', margin: 'auto' }}
                            className='p-3'
                        />
                        <Card.Subtitle className='text-center mt-auto'>{product.name}</Card.Subtitle>
                        <Card.Text className='text-center m-3 ' style={{ fontSize: '14px' }}>
                            ₱ {product.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}
                        </Card.Text>
                        <Card.Text>{product.description}</Card.Text>
                    </Card.Body>
                    <div className='card-overlay'>
                        <Col className='d-flex justify-content-center'>
                            <UpdateProduct product={product._id} fetchData={fetchData} />
                        </Col>
                        <Col className='d-flex justify-content-center'>
                            <ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} />
                        </Col>
                    </div>
                </Card>

            )



        })

        setProducts(productsArr)

    }, [productsData])


    return (
        <>
            <h1 className="text-center my-4"> Admin Dashboard</h1>
            <Row >
                <Col className="d-flex align-items-center">
                    {/* <div>Products grid</div> */}
                    <AddProduct fetchData={fetchData} />
                </Col>
            </Row>

            <Row >
                <Col className="d-flex align-items-center">
                   {/*  <div>(Search Compnent HERE)</div> */}

                    {/* <div className='ms-auto'>(Category Component HERE)</div> */}
                    {/* <div>(Last Added - Sort Component HERE)</div> */}
                </Col>
            </Row>

            <Row className='justify-content-center p-5'>
            {isLoading ? (
						// Loading component goes here
						<div className="spinner-border" role="status">
							<span className="sr-only"></span>
						</div>
					) : (
						// Render orders when data is available
						products
					)}
            </Row>
        </>
    )
}
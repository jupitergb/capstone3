import React, { useEffect } from 'react';
import { useContext, useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { FaShoppingCart } from 'react-icons/fa'; // Import the cart icon

import Container from 'react-bootstrap/Container';


export default function CustomNavbar() {

  const { user } = useContext(UserContext);
  const { cart, setCart } = useContext(UserContext);






  return (
    <Navbar bg="dark" variant="dark" expand="md">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">JMobile</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>

            {(user.id !== null) ?

              user.isAdmin
                ?
                <>
                  <Nav.Link as={Link} to="/products">Dashboard</Nav.Link>
                  <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                </>
                :
                <>
                  
                  <Nav.Link className='mx-2' as={Link} to="/products">Shop Products</Nav.Link>
                  <Nav.Link className='mx-2' as={Link} to="/cart">
                    <FaShoppingCart /> Cart <span className='text-danger'>({cart.products !== undefined ? cart.products.length : 0})</span>
                  </Nav.Link>
                  <Nav.Link className='mx-2' as={Link} to="/profile">Profile</Nav.Link>
                  <Nav.Link className='mx-2' as={Link} to="/logout">Logout</Nav.Link>
                </>
              :
              <>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
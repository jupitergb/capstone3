import { useState, useEffect } from 'react'
import { BrowserRouter as Router, useLocation } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Container from 'react-bootstrap/Container'



import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Products from './pages/Products';
import Cart from './pages/Cart';
import Error from './pages/Error';
import AppNavbar from './components/AppNavbar';


import { UserProvider } from './UserContext';
import Checkout from './pages/Checkout';
import Confirmation from './pages/Confirmation';
import Profile from './pages/Profile';
import SingleProduct from './pages/SingleProduct';
import Footer from './components/Footer';


function App() {




  const [user, setUser] = useState({
    id: null,
    firstName: null,
    lastName: null,
    email: null,
    isAdmin: null
  });

  const [cart, setCart] = useState({
    products: [],
    totalAmount: 0
  });




  const unsetUser = () => {

    localStorage.clear();

  };


  const fetchCart = () => {

    fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')} `
      }
    })
      .then(res => res.json())
      .then(data => {
        setCart(data);
      })
  }


  useEffect(() => {


    fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if (typeof data.id !== "undefined") {

          setUser({
            id: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            isAdmin: data.isAdmin
          });

        } else {

          setUser({
            id: null,
            firstName: null,
            lastName: null,
            email: null,
            isAdmin: null
          });

        }

      })

    fetchCart();

  }, []);




  return (
    <UserProvider value={{ user, setUser, unsetUser, cart, setCart }}>
      <Router>
        <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
          <AppNavbar />

          <Container fluid style={{ flex: 1 }}>

            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/home" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/single-product/:productId" element={<SingleProduct />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/checkout" element={<Checkout />} />
              <Route path="/confirmation" element={<Confirmation />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<Error />} />

            </Routes>



          </Container>
          <Footer style={{ position: 'absolute', bottom: 0, width: '100%' }} />
        </div>

      </Router>
    </UserProvider>


  );
}

export default App;

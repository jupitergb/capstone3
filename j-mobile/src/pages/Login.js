import { useState, useContext, useEffect } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link, Navigate } from 'react-router-dom';


export default function Login() {


    const { user, setUser } = useContext(UserContext);


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {


        e.preventDefault();
        fetch('https://cpstn2-ecommerceapi-baliguat.onrender.com/users/login', {

            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({

                email: email,
                password: password

            })
        })
            .then(res => res.json())
            .then(data => {

                if (typeof data.accessToken !== "undefined") {

                    localStorage.setItem('token', data.accessToken);


                    retrieveUserDetails(data.accessToken);

                    setUser({
                        accessToken: localStorage.getItem('token')
                    })

                    Swal.fire({
                        title: "Login successful",
                        icon: "success",
                        text: "Welcome to JMobile!"
                    })



                } else {
                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: data.message
                    })
                }
            })

        setEmail('');
        setPassword('');


    }

    const retrieveUserDetails = (token) => {
        fetch('https://cpstn2-ecommerceapi-baliguat.onrender.com/users/profile', {
            headers: {
                Authorization: `Bearer ${token} `
            }
        })
            .then(res => res.json())
            .then(data => {

                setUser({
                    id: data.id,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    isAdmin: data.isAdmin
                })
            })
    }

    useEffect(() => {

        
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, password]);


    return (

        (user.id !== null) ?

			<Navigate to="/products" />

			:


        <Row className="vh-100 align-items-center justify-content-center">
            <Col xs={10} sm={8} md={6} lg={4}>
                <Form className='border rounded px-3 py-4' onSubmit={(e) => authenticate(e)}>

                    <h1 style={{ fontSize: '24px', fontWeight: 'bold' }}>Sign in</h1>
                    <p style={{ fontSize: '12px' }}>
                        New customer? <Link to='/register' style={{ color: 'inherit' }}>Create new account</Link>
                    </p>

                    <Form.Group className="mb-3 mt-5" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder=""
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder=""
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                            autoComplete='true' />
                    </Form.Group>
                    <p className='text-end' style={{ fontSize: '12px' }}><a href='#' style={{ color: 'inherit' }}>Forgot info</a></p>


                    {isActive ?
                        <Button variant="dark" style={{ width: '100%' }} type="submit" className='my-3'>
                            Sign in
                        </Button>
                        :
                        <Button variant="secondary" style={{ width: '100%' }} type="submit" className='my-3' disabled>
                            Sign in
                        </Button>
                    }
                </Form>
            </Col>
        </Row>
    );
}

import { useState, useContext, useEffect } from 'react'

import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link, Navigate } from 'react-router-dom';

export default function Register() {


  const { user } = useContext(UserContext);

  const navigate = useNavigate();


  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [isActive, setIsActive] = useState(false);



  function registerUser(e) {


    e.preventDefault();

    fetch('https://cpstn2-ecommerceapi-baliguat.onrender.com/users/register', {

      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({

        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password

      })
    })
      .then(res => res.json())
      .then(data => {
        if (data) {

          setFirstName('');
          setLastName('');
          setEmail('');
          setPassword('');
          setConfirmPassword('');


          Swal.fire({
            title: "Registered successfully",
            icon: "success",
            text: "You may now login"
          })

          navigate('/login');
          

        } else {

          Swal.fire({
            title: "Failed",
            icon: "error",
            text: data.message
          })
        }

      })
  }



  useEffect(() => {

    if ((firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)) {

      setIsActive(true)

    } else {

      setIsActive(false)

    }

  }, [firstName, lastName, email, password, confirmPassword])


  return (

    (user.id !== null) ?

			<Navigate to="/products" />

			:


    <Row className="vh-100 align-items-center justify-content-center">
      <Col xs={10} sm={8} md={6} lg={4}>
        <Form className='border rounded px-3 py-4' onSubmit={(e) => registerUser(e)}>
          <h1 style={{ fontSize: '24px', fontWeight: 'bold' }}>Register</h1>
          <p style={{ fontSize: '12px' }}>
            Already have an account? <Link to='/login' style={{ color: 'inherit' }}>Sign in</Link>
          </p>

          <Row className="mb-3 mt-5">
            <Col>
              <Form.Group controlId="formFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="" required
                  value={firstName}
                  onChange={e => { setFirstName(e.target.value) }}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group controlId="formLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder=""
                  required
                  value={lastName}
                  onChange={e => { setLastName(e.target.value) }} />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group className="mb-3" controlId="formEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder=""
              required
              value={email}
              onChange={e => { setEmail(e.target.value) }} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder=""
              required
              value={password}
              onChange={e => { setPassword(e.target.value) }} 
              autoComplete='true'/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formConfirmPassword">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              type="password"
              placeholder=""
              required
              value={confirmPassword}
              onChange={e => { setConfirmPassword(e.target.value) }} 
              autoComplete='true'/>
          </Form.Group>

          {
            isActive

              ? <Button variant="dark" style={{ width: '100%' }} type="submit" className='my-4'>
                Register
              </Button>
              : <Button variant="secondary" style={{ width: '100%' }} type="submit" className='my-4' disabled>
                Register
              </Button>
          }


        </Form>
      </Col>
    </Row>
  );
}

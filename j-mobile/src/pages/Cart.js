import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Container, Row, Table } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Cart() {

    const { cart, setCart } = useContext(UserContext);


    const [productsData, setProducts] = useState([])

    const fetchCart = () => {

        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setCart(data);
            })
    }

    const changeQuantity = (productId, quantity) => {
        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/change/${productId}/${quantity}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                if (data.status === true) {
                    fetchCart();


                } else {
                    Swal.fire({
                        title: 'Error!',
                        icon: 'error',
                        text: data.message
                    })
                    fetchCart();
                }
            })
    }

    const handleChangeQuantity = (productId, productName, quantity) => {

        if (quantity === 0) {
            Swal.fire({
                title: 'Confirm',
                text: `Are you sure you want to remove "${productName}"?`,
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
            }).then((result) => {
                if (result.isConfirmed) {

                    changeQuantity(productId, quantity);
                    Swal.fire('Confirmed', `${productName} removed from your Cart`, 'success');

                } else {

                    Swal.fire('Cancelled', `${productName} not removed.`, 'info');
                    fetchCart();
                }
            });
        } else {
            changeQuantity(productId, quantity);
        }



    }

    const handleRemoveItem = (productId, productName) => {


        Swal.fire({
            title: 'Confirm',
            text: `Are you sure you want to remove "${productName}"?`,
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        }).then((result) => {
            if (result.isConfirmed) {

                fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/remove/${productId}`, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then(res => res.json())
                    .then(data => {

                        if (data.status === true) {
                            Swal.fire('Confirmed', `${productName} removed from your Cart`, 'success');
                            fetchCart();

                        } else {
                            Swal.fire({
                                title: 'Error!',
                                icon: 'error',
                                text: data.message
                            })
                            fetchCart();
                        }
                    })


            } else {

                Swal.fire('Cancelled', `${productName} not removed.`, 'info');
                fetchCart();
            }
        });
    }

    useEffect(() => {
        const productsArr = cart.products.map(product => {
            return (
                <>
                    <tr key={product.id}>
                        <td className='text-center align-middle'><img src={product.image} height={"100px"} /></td>
                        <td className='text-center align-middle'> {product.name}</td>
                        <td className='text-center align-middle'>₱ {product.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</td>
                        <td className='text-center align-middle'><Button variant='danger' size='sm' onClick={() => handleChangeQuantity(product.productId, product.name, product.quantity - 1)}>-</Button>  {product.quantity}  <Button variant='primary' size='sm' onClick={() => handleChangeQuantity(product.productId, product.name, product.quantity + 1)}>+</Button></td>
                        <td className='text-center align-middle'>₱ {product.subTotal.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</td>
                        <td className='text-danger text-center align-middle'><Button variant='danger' size='sm' onClick={() => handleRemoveItem(product.productId, product.name)}>Remove</Button> </td>
                    </tr>


                </>

            )
        })

        setProducts(productsArr)

    }, [cart])


    return (
        <>

<Container>
			<Breadcrumb className="mt-3">
				<Breadcrumb.Item href="/">Home</Breadcrumb.Item>
				<Breadcrumb.Item href="/products">Products</Breadcrumb.Item>
                <Breadcrumb.Item active>Cart</Breadcrumb.Item>
			</Breadcrumb>
            <h1 className="text-center my-4"> Cart</h1>

            <Table hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>Item Image</th>
                        <th>Item Name</th>
                        <th>Item Price</th>
                        <th>Item Quantity</th>
                        <th>Item Subtotal</th>
                        <th colSpan="2">Actions</th>
                    </tr>

                </thead>

                <tbody>
                    {productsData}
                </tbody>
            </Table>

            <Row className='justify-content-end me-5 fs-5 fw-medium'>Total Amount: {cart.totalAmount === undefined ? 0 : `₱ ${cart.totalAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}`}</Row>
            <Row className='m-3' >{cart.totalAmount === undefined 
                                    ? 
                                    
                                        <Button variant='info' disabled>Checkout</Button> 
                                    
                                    :                                     
            
                                    <Link to="/checkout" className="col-12">
                                        <Button variant='info'  style={{ width: '100%' }}>Checkout</Button>
                                  </Link>}
            </Row>
            </Container>
            
        </>

    )
}
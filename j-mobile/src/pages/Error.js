import React from 'react'
import Col from 'react-bootstrap/esm/Col'
import Row from 'react-bootstrap/esm/Row'
import { Link } from 'react-router-dom'

export default function Error() {
    return (
        <>
            <Row>
                <Col className="pt-5 ps-5">
                    <h3 className='fw-bold'>We're sorry</h3>
                    <h2 className="fw-bold py-3" style={{fontSize: '48px'}}>Oops! The page you're looking for is no longer available.</h2>
                    <p style={{fontSize: '14px'}}><strong>Bad timing, we know.</strong></p>
                </Col>
            </Row>
            <Row>
                <Col className='ps-5 pt-3'>
                    <Link to="/" style={{color: 'inherit'}} tabindex="0" className='fw-bold'>Go to Home</Link>
                </Col>
            </Row>
        </>
    )
}

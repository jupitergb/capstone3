import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { Container, Row, Col, Card, Image, Nav, Tab, Button, Accordion, Table } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Profile() {

    const { user } = useContext(UserContext);
    const [myOrders, setMyOrders] = useState([]);
    const [allOrders, setAllOrders] = useState([]);
    const [orders, setOrders] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [users, setUsers] = useState([]);
    const [usersData, setUsersData] = useState([]);
    const uniqueUserIds = [...new Set(allOrders.map(order => order.userId))];


    const fetchMyOrders = (userId) => {
        setMyOrders([]);
        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/myOrders/${userId}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setMyOrders(data);
            })
    }

    const fetchProfile = (userId) => {
        return new Promise((resolve, reject) => {
            fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/users/profile/${userId}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')} `
                }
            })
                .then(res => res.json())
                .then(data => {
                    resolve(data); // Resolve the Promise with the data
                })
                .catch(error => {
                    reject(error); // Reject the Promise with an error if fetch fails
                });
        });
    }

    const fetchAllUsers = () => {
        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/users/profile/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setUsers(data);
            })
    }


    const fetchAllOrders = () => {

        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setAllOrders(data);
            })
    }


    useEffect(() => {
        if (user.isAdmin === false) {
            fetchMyOrders(user.id);
        } else {
            fetchAllOrders();
            fetchAllUsers();
        }
    }, [])

    useEffect(() => {
        if (myOrders.length > 0) {
            setIsLoading(false); // Data is loaded
        }
    }, [myOrders]);

    useEffect(() => {
        if (allOrders.length > 0) {
            setIsLoading(false); // Data is loaded
        } 
    }, [allOrders]);





    useEffect(() => {
        if (user.isAdmin === false && myOrders) {

            const ordersArr = myOrders.map(order => {
                const orderDate = new Date(order.orderDate);
                const formattedDate = `${orderDate.toLocaleDateString('en-US', {
                    year: 'numeric',
                    month: '2-digit',
                    day: '2-digit',
                })} ${orderDate.toLocaleTimeString('en-US', {
                    hour: '2-digit',
                    minute: '2-digit',
                })}`;
                return (

                    <Row className="text-center d-flex justify-content-center align-items-center" key={order._id}>
                        <Row className='justify-content-end text-info text-end me-md-3 ' style={{ fontSize: "14px" }} >Order ID: {order._id}</Row>
                        <Row className='justify-content-end text-info text-end me-md-3' style={{ fontSize: "14px" }} >Order Date: {formattedDate}</Row>
                        <Row className='justify-content-end text-secondary text-end me-md-3' style={{ fontSize: "14px" }}>
                            Shipped to: {order.shippingAddress}
                        </Row>


                        <Col md="6">
                            {order.products.map(product => (
                                <Row className='align-items-center my-5' key={product._id}>

                                    <Col md="3"><img className='w-50 mb-0' src={product.image} alt={product.name} /></Col>
                                    <Col md="5">
                                        <Row className="justify-content-center" style={{ fontSize: "14px" }} >{product.name}</Row>
                                        <Row className="justify-content-center text-secondary" style={{ fontSize: "14px" }}>Quantity: {product.quantity}</Row>
                                    </Col>
                                    <Col style={{ fontSize: "14px" }} md="4">₱ {product.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</Col>
                                </Row>
                            ))}
                            <Row className='justify-content-md-end fw-semibold justify-content-center text-danger mb-5 me-md-3'>
                                Total: ₱ {order.totalAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}
                            </Row>
                        </Col>
                        <div className="border-bottom border-black border-1 mb-4"></div>

                    </Row>


                )
            })
            setOrders(ordersArr)
        }

    }, [myOrders])


    useEffect(() => {

        if (user.isAdmin === true && myOrders) {

            const userIdToNameMap = {}; // Create a map to store user names
            const fetchPromises = []; // Create an array to store fetch promises

            // Create fetch promises for all unique user IDs
            uniqueUserIds.forEach(userId => {
                const fetchPromise = fetchProfile(userId)
                    .then(data => {
                        userIdToNameMap[userId] = `${data.firstName} ${data.lastName}`; // Store user name in the map
                    })
                    .catch(error => {
                        console.error(`Error fetching profile for user ${userId}:`, error);
                    });
                fetchPromises.push(fetchPromise);
            });

            // Wait for all fetch promises to complete
            Promise.all(fetchPromises)
                .then(() => {
                    const ordersArr = uniqueUserIds.map((userId) => (
                        <Accordion.Item eventKey={userId} key={userId}>
                            <Accordion.Header onClick={() => { fetchMyOrders(userId) }}>{userIdToNameMap[userId]}</Accordion.Header>
                            {myOrders.map(order => (

                                <Accordion.Body key={order._id}>

                                    <Row className="text-center d-flex justify-content-center align-items-center">
                                        <Row className='justify-content-end text-info text-end me-md-3 ' style={{ fontSize: "14px" }} >Order ID: {order._id}</Row>
                                        <Row className='justify-content-end text-info text-end me-md-3' style={{ fontSize: "14px" }} >Order Date: {order.orderDate}</Row>
                                        <Row className='justify-content-end text-secondary text-end me-md-3' style={{ fontSize: "14px" }}>
                                            Shipped to: {order.shippingAddress}
                                        </Row>
                                        <Col md="6">

                                            {order.products.map(product => (
                                                <Row className='align-items-center my-5' key={product._id} >
                                                    <Col md="3"><img className='w-50 mb-0' src={product.image} alt={product.name} /></Col>
                                                    <Col md="5">
                                                        <Row className="justify-content-center" style={{ fontSize: "14px" }} >{product.name}</Row>
                                                        <Row className="justify-content-center text-secondary" style={{ fontSize: "14px" }}>Quantity: {product.quantity}</Row>
                                                    </Col>
                                                    <Col style={{ fontSize: "14px" }} md="4">₱ {product.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</Col>
                                                </Row>
                                            ))}
                                            <Row className='justify-content-md-end fw-semibold justify-content-center text-danger mb-5 me-md-3'>
                                                Total: ₱ {order.totalAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}
                                            </Row>
                                        </Col>
                                        <div className="border-bottom border-black border-1 mb-4"></div>
                                    </Row>
                                </Accordion.Body>
                            ))}
                        </Accordion.Item>
                    ));

                    setOrders(ordersArr);
                })
                .catch(error => {
                    console.error("Error fetching profiles:", error);
                });
        }
    }, [myOrders, allOrders]);





    const updateUserRole = (userId, isAdmin) => {

        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/users/${userId}/${isAdmin}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                if (data.status === true) {
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'Role Successfully Updated'
                    })
                    fetchAllUsers();

                } else {
                    Swal.fire({
                        title: 'Error!',
                        icon: 'error',
                        text: 'Please try again'
                    })
                    fetchAllUsers();
                }
            })
    }


    useEffect(() => {
        const usersArr = users.map(user => {

            return (
                <>
                    <tr key={user._id}>
                        <td className='text-center align-middle'>{user._id}</td>
                        <td className='text-center align-middle'> {user.firstName}</td>
                        <td className='text-center align-middle'>{user.lastName}</td>
                        <td className='text-center align-middle'>{user.email}</td>
                        <td className='text-center align-middle'>{user.isAdmin ? <div className='text-danger'>Admin</div> : <div className='text-secondary'>User</div>}</td>
                        <td className='text-danger text-center align-middle'>
                            {user.isAdmin ?
                                <Button variant='primary' size='sm' onClick={() => { updateUserRole(user._id, false) }}>Make a User</Button>
                                :
                                <Button variant='danger' size='sm' onClick={() => { updateUserRole(user._id, true) }}>Make an Admin</Button>} </td>
                    </tr>


                </>

            )
        })

        setUsersData(usersArr)

    }, [users])

    return (
        (
            (user.id === null) ?

                <Navigate to="/login" />

                :
                <section className="vh-100" style={{ backgroundColor: '#f4f5f7' }}>
                    <Container className="py-5 h-100">
                        <Tab.Container id="profile-tabs" defaultActiveKey="profile">
                            <Row className="justify-content-center align-items-center h-100">
                                <Col className="mb-4 mb-0">
                                    <Card className="mb-3" style={{ borderRadius: '.5rem' }}>
                                        <Row className="g-0">
                                            <Col xs="12" className="gradient-custom text-center text-white pb-5"
                                                style={{ borderTopLeftRadius: '.5rem', borderBottomLeftRadius: '.5rem' }}>
                                                <Image src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava1-bg.webp"
                                                    alt="Avatar" className="my-5" style={{ width: '80px' }} fluid />
                                                <h5>{user.firstName} {user.lastName}</h5>
                                                {/* <FaEdit className="mb-5" /> */}
                                            </Col>
                                            <Col xs="12">
                                                <Card.Body className="p-4">
                                                    <Nav variant="tabs" className="mb-4">
                                                        <Nav.Item>
                                                            <Nav.Link eventKey="profile">Profile Details</Nav.Link>
                                                        </Nav.Item>
                                                        <Nav.Item>
                                                            <Nav.Link eventKey="orders">{user.isAdmin ? "All Orders" : "My Orders"}</Nav.Link>
                                                        </Nav.Item>

                                                        {user.isAdmin ? <Nav.Item>
                                                            <Nav.Link eventKey="users">All Users</Nav.Link>
                                                        </Nav.Item> : null}
                                                    </Nav>

                                                    <Tab.Content>
                                                        <Tab.Pane eventKey="profile">
                                                            <h6>Information</h6>
                                                            <hr className="mt-0 mb-4" />
                                                            <Row className="pt-1">
                                                                <Col size="6" className="mb-3">
                                                                    <h6>Email</h6>
                                                                    <p className="text-muted">{user.email}</p>
                                                                </Col>
                                                                <Col size="6" className="mb-3">
                                                                    <h6>Role</h6>
                                                                    <p className="text-muted">{user.isAdmin ? "Admin" : "User"}</p>
                                                                </Col>
                                                            </Row>
                                                        </Tab.Pane>
                                                        <Tab.Pane eventKey="orders">
                                                            {/* !myOrders ? "You don't have any orders yet. Shop now!" : */
                                                                isLoading ? (
                                                                    // Loading component goes here

                                                                    "No orders yet!"
                                                                ) :

                                                                    user.isAdmin ?
                                                                        (
                                                                            // Render orders when data is available
                                                                            <Accordion defaultActiveKey="0">

                                                                                {orders}

                                                                            </Accordion>
                                                                        ) : orders}


                                                        </Tab.Pane>
                                                        <Tab.Pane eventKey="users">
                                                            <Table hover responsive>
                                                                <thead>
                                                                    <tr className="text-center">
                                                                        <th>User ID</th>
                                                                        <th>First Name</th>
                                                                        <th>Last Name</th>
                                                                        <th>Email Address</th>
                                                                        <th>Role</th>
                                                                        <th colSpan="2">Actions</th>
                                                                    </tr>

                                                                </thead>

                                                                <tbody>
                                                                    {usersData}
                                                                </tbody>
                                                            </Table>



                                                        </Tab.Pane>
                                                    </Tab.Content>
                                                </Card.Body>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </Container>
                </section>)
    )
}

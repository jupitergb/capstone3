import React from 'react'
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { Navigate } from 'react-router-dom';
import { Breadcrumb, Card, Col, Container, Row, Table } from 'react-bootstrap';


export default function ConfirmationPage() {

    const { cart, setCart } = useContext(UserContext);
    const [myCart, setMyCart] = useState({ ...cart });
    const { user } = useContext(UserContext);
    const [ recentOrder, setRecentOrder ] = useState({});

    const fetchCart = () => {
        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setCart(data);
            });
    }

    const fetchRecentOrder = () => {
        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/recent`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setRecentOrder(data);
                console.log(data)
            });
    }

    useEffect(() => {
        fetchCart();
        fetchRecentOrder();
    }, []);



    const [productsData, setProducts] = useState([])

    useEffect(() => {
        const productsArr = myCart.products.map(product => {
            return (

                <tr>
                    <td width="20%" className="align-middle">
                        <img src={product.image} width="90" />
                    </td>

                    <td width="60%" className="align-middle">
                        <span className="fw-bold">{product.name}</span>
                        <div className="product-qty">
                            <span className="d-block">Quantity: {product.quantity}</span>


                        </div>
                    </td>
                    <td width="20%" className="align-middle">
                        <div className="text-right">
                            <span className="fw-bold">₱ {product.subTotal.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')} </span>
                        </div>
                    </td>
                </tr>


            )
        })

        setProducts(productsArr)

    }, [cart])

    const originalDate = new Date(recentOrder.orderDate);

    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const formattedDate = originalDate.toLocaleDateString('en-US', options);


    return (

        (myCart.products.length === 0) ?

            <Navigate to="/products" />

            :

            <Container className="mt-5 mb-5">
            <Breadcrumb className="mt-3">
				<Breadcrumb.Item href="/">Home</Breadcrumb.Item>
				<Breadcrumb.Item href="/products">Products</Breadcrumb.Item>
                <Breadcrumb.Item href="/cart">Cart</Breadcrumb.Item>
                <Breadcrumb.Item active>Order Confirmation</Breadcrumb.Item>
			</Breadcrumb>
                <Row className="d-flex justify-content-center">
                    <Col className="col-md-8">
                        <Card>
                           

                            <div className="invoice p-5">

                                <h5>Your order is Confirmed!</h5>

                                <span className="fw-bold d-block mt-4">Hello {user.firstName} {user.lastName}, </span>
                                <span>You order has been confirmed and will be shipped in next two days!</span>

                                <div className="payment border-top mt-3 mb-3 border-bottom table-responsive">
                                    <Table className="table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div className="py-2">
                                                        <span className="d-block text-muted">Order Date</span>
                                                        <span>{formattedDate}</span>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div className="py-2">
                                                        <span className="d-block text-muted">Order No</span>
                                                        <span>{recentOrder._id}</span>
                                                    </div>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                    <div className="py-2">
                                                        <span className="d-block text-muted">Payment</span>
                                                        <span>Cash on Delivery (COD)</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="py-2">
                                                        <span className="d-block text-muted">Shiping Address</span>
                                                        <span>{recentOrder.shippingAddress}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>

                                <div className="product border-bottom table-responsive">
                                    <Table className="table-borderless">
                                        <tbody>
                                            {productsData}
                                        </tbody>
                                    </Table>
                                </div>

                                <Row className="d-flex justify-content-end">
                                    <Col className="col-md-5">
                                        <Table className="table-borderless">
                                            <tbody className="totals">
                                                <tr>
                                                    <td>
                                                        <div className="text-left">
                                                            <span className="text-muted">Subtotal</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="text-right">
                                                            <span>₱ {myCart.totalAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</span>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div className="text-left">
                                                            <span className="text-muted">Shipping Fee</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="text-right">
                                                            <span>FREE</span>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr className="border-top border-bottom">
                                                    <td>
                                                        <div className="text-left">
                                                            <span className="fw-bold">TOTAL</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className="text-right">
                                                            <span className="fw-bold">₱ {myCart.totalAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</span>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>

                                <p>We will be sending shipping confirmation email when the item shipped successfully!</p>
                                <p className="fw-bold mb-0">Thanks for shopping with us!</p>
                                <span>JMobile Team</span>
                            </div>

                        </Card>
                    </Col>
                </Row>
            </Container>)
}

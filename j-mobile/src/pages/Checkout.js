import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Row, Col, Form, Button, Container, Breadcrumb } from 'react-bootstrap';
import { Link, Route, Routes } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { regions, provinces, cities, barangays } from 'select-philippines-address';



export default function Checkout() {

    const { user } = useContext(UserContext);

    const [fullName, setFullName] = useState(`${user.firstName} ${user.lastName}`);
    const [street, setStreet] = useState("");
    const [shippingAddress, setShippingAddress] = useState("");

    const handleStreetChange = (e) => {
        const newStreet = e.target.value;
        setStreet(newStreet);

        // Update the shipping address
        const newShippingAddress = `${newStreet}, ${barangayAddr}, ${cityAddr}, ${provinceAddr}, ${regionAddr}`;
        setShippingAddress(newShippingAddress);
    };

    const handleNameChange = (e) => {
        const newFullName = e.target.value;
        setFullName(newFullName);
    };

    const [regionData, setRegion] = useState([]);
    const [provinceData, setProvince] = useState([]);
    const [cityData, setCity] = useState([]);
    const [barangayData, setBarangay] = useState([]);

    const [regionAddr, setRegionAddr] = useState("");
    const [provinceAddr, setProvinceAddr] = useState("");
    const [cityAddr, setCityAddr] = useState("");
    const [barangayAddr, setBarangayAddr] = useState("");

    const region = () => {
        regions().then(response => {
            setRegion(response);
        });
    }

    const province = (e) => {
        setRegionAddr(e.target.selectedOptions[0].text);
        provinces(e.target.value).then(response => {
            setProvince(response);
            setCity([]);
            setBarangay([]);
        });
    }

    const city = (e) => {
        setProvinceAddr(e.target.selectedOptions[0].text);
        cities(e.target.value).then(response => {
            setCity(response);
        });
    }

    const barangay = (e) => {
        setCityAddr(e.target.selectedOptions[0].text);
        barangays(e.target.value).then(response => {
            setBarangay(response);
        });
    }

    const brgy = (e) => {
        setBarangayAddr(e.target.selectedOptions[0].text);
    }

    useEffect(() => {
        region()
    }, [])

    const { cart, setCart } = useContext(UserContext);
    const [productsData, setProducts] = useState([])

    const fetchCart = () => {

        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')} `
            }
        })
            .then(res => res.json())
            .then(data => {
                setCart(data);
            })
    }

    const handleRemoveItem = (productId, productName) => {


        Swal.fire({
            title: 'Confirm',
            text: `Are you sure you want to remove "${productName}"?`,
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        }).then((result) => {
            if (result.isConfirmed) {

                fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/remove/${productId}`, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then(res => res.json())
                    .then(data => {

                        if (data.status === true) {
                            Swal.fire('Confirmed', `${productName} removed from your Cart`, 'success');
                            fetchCart();

                        } else {
                            Swal.fire({
                                title: 'Error!',
                                icon: 'error',
                                text: data.message
                            })
                            fetchCart();
                        }
                    })


            } else {

                Swal.fire('Cancelled', `${productName} not removed.`, 'info');
                fetchCart();
            }
        });
    }



    useEffect(() => {
        const productsArr = cart.products.map(product => {
            return (
                <>

                    <Row >
                        <hr />
                        {/* Item Image */}
                        <Col xs={5} className='d-flex justify-content-center align-items-center'>
                            <img src={product.image} height={"100px"} alt="Item"/>
                        </Col>
                        {/* Item Description */}
                        <Col xs={7} >
                            <p className='fs-6 fw-semibold'>{product.name}</p>
                            <p className='fs-6 text-secondary mb-1'>{product.quantity} x ₱ {product.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}</p>
                            <Button className='px-0 pb-3' variant="link" onClick={() => handleRemoveItem(product.productId, product.name)} style={{ textDecoration: "none", color: "red" }}>Remove</Button>
                        </Col>
                    </Row>

                </>
            )
        })

        setProducts(productsArr)

    }, [cart])

    const checkout = () => {

        fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/orders/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')} `
            },
            body: JSON.stringify({
                shippingAddress: shippingAddress
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === true) {
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: data.message

                    })


                } else {
                    Swal.fire({
                        title: 'Error!',
                        icon: 'error',
                        text: data.details
                    })

                }
            })
    }



    return (

        

        (cart.products.length === 0) ?

            <Navigate to="/products" />

            :

<Container>
			<Breadcrumb className="mt-3">
				<Breadcrumb.Item href="/">Home</Breadcrumb.Item>
				<Breadcrumb.Item href="/products">Products</Breadcrumb.Item>
                <Breadcrumb.Item href="/cart">Cart</Breadcrumb.Item>
				<Breadcrumb.Item active>Checkout</Breadcrumb.Item>
			</Breadcrumb>
        <Row className='m-5'>
            {/* First Column: Billing Details and Payment Method */}
            <Col md={8}>
                <h2>Shipping Details</h2>
                <hr />
                <Form className='m-5'>
                    {/* Full Name */}
                    <Form.Group className='mt-3' controlId="fullName">
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder=""
                            value={fullName}
                            onChange={handleNameChange}
                            required
                            disabled
                        />
                    </Form.Group>

                    {/* Region */}
                    <Form.Group className='mt-3' controlId="region">
                        <Form.Label >Region</Form.Label>
                        <Form.Select onChange={province} onSelect={region}>
                            <option disabled>Select Region</option>
                            {
                                regionData && regionData.length > 0 && regionData.map((item) => <option
                                    key={item.region_code} value={item.region_code}>{item.region_name}</option>)
                            }</Form.Select>
                    </Form.Group>

                    {/* Province */}
                    <Form.Group className='mt-3' controlId="province">
                        <Form.Label >Province</Form.Label>
                        <Form.Select onChange={city}>
                            <option disabled>Select Province</option>
                            {
                                provinceData && provinceData.length > 0 && provinceData.map((item) => <option
                                    key={item.province_code} value={item.province_code}>{item.province_name}</option>)
                            }</Form.Select>
                    </Form.Group>

                    {/* City */}
                    <Form.Group className='mt-3' controlId="city">
                        <Form.Label >City</Form.Label>
                        <Form.Select onChange={barangay}>
                            <option disabled>Select City</option>
                            {
                                cityData && cityData.length > 0 && cityData.map((item) => <option
                                    key={item.city_code} value={item.city_code}>{item.city_name}</option>)
                            }</Form.Select>
                    </Form.Group>

                    {/* Barangay */}
                    <Form.Group className='mt-3' controlId="barangay">
                        <Form.Label >Barangay</Form.Label>
                        <Form.Select onChange={brgy}>
                            <option disabled>Select Barangay</option>
                            {
                                barangayData && barangayData.length > 0 && barangayData.map((item) => <option
                                    key={item.brgy_code} value={item.brgy_code}>{item.brgy_name}</option>)
                            }</Form.Select>
                    </Form.Group>




                    {barangayAddr && cityAddr && provinceAddr && regionAddr
                        ?
                            <Form.Group className='mt-3' controlId="street">
                                <Form.Label>Street Address</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={street}
                                    onChange={handleStreetChange}
                                    required
                                />
                            </Form.Group>
                        :

                            <Form.Group className='mt-3' controlId="street">
                                <Form.Label>Street Address</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder=""
                                    value={street}
                                    onChange={handleStreetChange}
                                    required
                                    disabled
                                />
                            </Form.Group>
                    }

                </Form>

            </Col>

            {/* Second Column: Order Summary */}
            <Col md={4}>
                <h2>Order Summary</h2>
                {productsData}
                <hr />

                {/* Shipping Fee */}
                <Row>
                    <Col xs={7}>
                        <p>Shipping:</p>
                    </Col>
                    <Col xs={5}>
                        <p>FREE</p>
                    </Col>
                </Row>




                {/* Shipping Address */}
                <Row>
                    <Col xs={7}>
                        <p>Shipping Address:</p>
                    </Col>
                    <Col xs={5}>
                        <p>{fullName}</p>
                        <p>{shippingAddress}</p>
                    </Col>
                </Row>

                <hr />

                {/* Total Amount */}
                <Row>
                    <Col xs={7}>
                        <h6>Total Amount</h6>
                    </Col>
                    <Col xs={5}>
                        <h6 className='fw-bold text-danger'>{cart.totalAmount === undefined ? 0 : `₱ ${cart.totalAmount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ')}`}</h6>
                    </Col>
                </Row>

                {street && barangayAddr && cityAddr && provinceAddr && regionAddr && fullName
                    ?

                    <Link to="/confirmation" className="col-12">
                        <Button variant='info' style={{ width: '100%' }} className='mt-3' onClick={() => { checkout() }}>Place Order</Button>
                    </Link>

                    :

                    <Button variant='info' disabled style={{ width: '100%' }} className='mt-3'>Place Order</Button>
                }
            </Col>
        </Row>
        </Container>
    );
}

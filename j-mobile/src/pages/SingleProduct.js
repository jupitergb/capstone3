import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useParams } from 'react-router-dom';
import { Breadcrumb, Container, Row, Col, Image, Tabs, Tab, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const SingleProduct = () => {

  const { user } = useContext(UserContext);
  const { setCart } = useContext(UserContext);

  const { productId } = useParams();
  const [selectedProduct, setSelectedProduct] = useState({});
  const [loading, setLoading] = useState(true);

  const [value, setValue] = useState(1); // Initial value

  const handleIncrement = () => {
    setValue(value + 1);
  };

  const handleDecrement = () => {
    if (value > 1) {
      setValue(value - 1);
    }
  };

  const product = {
    name: 'Sample Product',
    price: 100,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit...',
    imageSrc: 'sample-image.jpg',
  };

  const fetchSingleProduct = (productId) => {
    fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/products/retrieve/${productId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')} `
      }
    })
      .then(res => res.json())
      .then(data => {
        setSelectedProduct(data);
      })
  }

  const fetchCart = () => {

    fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')} `
      }
    })
      .then(res => res.json())
      .then(data => {
        setCart(data);
      })
  }

  const handleAddToCart = (productId, productName, quantity) => {

    if (user.id === null) {
      Swal.fire({
        title: 'Not Logged In!',
        icon: 'info',
        text: `You are not logged in. Log in now to add items to your cart!`
      })

    } else {
      fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/add/${productId}/${quantity}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {

          if (data.status === true) {
            Swal.fire({
              title: 'Success!',
              icon: 'success',
              text: `${quantity} x ${productName} added to cart`
            })
            fetchCart();

          } else {
            Swal.fire({
              title: 'Error!',
              icon: 'error',
              text: data.error
            })
            fetchCart();
          }
        })

    }


  }


  useEffect(() => {
    fetchSingleProduct(productId);
    setLoading(false);
  }, []);


  return (
    <>
      {user.isAdmin ? <Navigate to='/products' /> :
        <>{loading ? (
          // If loading is true, render the loading screen
          <div>Loading...</div>
        ) :


          <Container >
            <Breadcrumb className="mt-3">
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item href="/products">Products</Breadcrumb.Item>
              <Breadcrumb.Item active>{selectedProduct.name}</Breadcrumb.Item>
            </Breadcrumb>

            <Row className="mt-4">
              <Col xs={12} md={6} className='d-flex justify-content-center align-items-center'>
                <Image className='w-75' src={selectedProduct.image} alt={selectedProduct.name} />
              </Col>
              <Col xs={12} md={6}>
                <h2>{selectedProduct.name}</h2>
                <p>Price: ₱ {selectedProduct.price ? selectedProduct.price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ', ') : 0}</p>
                <p className='my-5'>{selectedProduct.description}</p>
                <div className="d-flex align-items-center">
                  <label className="mr-2">Quantity:</label>

                  <div className="d-flex align-items-center">
                    <Button variant="none" size="lg" onClick={handleDecrement}>
                      -
                    </Button>
                    <input
                      type="number"
                      value={value}
                      min="1"
                      className="form-control text-center"
                      style={{ width: '50px' }}
                      readOnly // Make the input read-only to prevent manual edits
                    />
                    <Button variant="none" size="lg" onClick={handleIncrement}>
                      +
                    </Button>
                  </div>
                </div>
                <Button className="mt-3" variant="primary" onClick={() => handleAddToCart(selectedProduct._id, selectedProduct.name, value)}>
                  Add to Cart
                </Button>
              </Col>
            </Row>

            <Tabs className="my-4" defaultActiveKey="description">
              <Tab eventKey="description" title="Product Description">
                {/* Replace with your product description */}
                <p>{selectedProduct.description}</p>
              </Tab>
              <Tab eventKey="reviews" title="Reviews">
                {/* Add your review content here */}
                <p>No reviews available for this product.</p>
              </Tab>
            </Tabs>
          </Container>}</>
      }
    </>
  );

};

export default SingleProduct;

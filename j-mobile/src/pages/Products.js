import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

export default function Products() {

	const { user } = useContext(UserContext);
	const { cart, setCart } = useContext(UserContext);

	const [products, setProducts] = useState([]);


	const fetchData = () => {

		user.isAdmin ?
				fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/products/retrieve/all`, {
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')} `
					}
				})
					.then(res => res.json())
					.then(data => {
						setProducts(data);
					})
			:
				fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/products/retrieve/active`, {
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')} `
					}
				})
					.then(res => res.json())
					.then(data => {
						setProducts(data);
					})


	}

	const fetchCart = () => {

		fetch(`https://cpstn2-ecommerceapi-baliguat.onrender.com/carts/view`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')} `
			}
		})
			.then(res => res.json())
			.then(data => {				
				setCart(data);
			})
	}



	useEffect(() => {

		fetchData()
		fetchCart();
	}, []);


	return (
		<>
			
			{
				
				(user.isAdmin === true) ?
					<AdminView productsData={products} fetchData={fetchData} />

					:

					<UserView productsData={products} fetchData={fetchData} />
			}

		</>
	)
}











